package com.misiontic.apiproductos.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.misiontic.apiproductos.entidades.Categoria;

@Repository
public interface CategoriaRepositorio extends JpaRepository<Categoria, Long> {

	@Query("SELECT c FROM Categoria c")
	public List<Categoria> obtenerCategorias();

}
