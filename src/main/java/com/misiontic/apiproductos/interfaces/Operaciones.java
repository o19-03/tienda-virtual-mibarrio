package com.misiontic.apiproductos.interfaces;

import java.util.List;

public interface Operaciones<T> {

	public abstract boolean agregar(T objeto);

	public abstract List<T> consultarTodos();

	public abstract boolean eliminar(Long codigo);

	public abstract boolean actualizar(T objeto);

	public abstract Integer cantidadRegistros();

	public abstract T buscar(Long codigo);

}
