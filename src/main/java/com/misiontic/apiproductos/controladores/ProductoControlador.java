package com.misiontic.apiproductos.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.misiontic.apiproductos.entidades.Categoria;
import com.misiontic.apiproductos.entidades.Producto;
import com.misiontic.apiproductos.servicios.ProductoServicio;

@RestController
@RequestMapping("/productos")
@CrossOrigin(origins = "*")
public class ProductoControlador {

	@Autowired
	private ProductoServicio prodServicio;

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/listado", method = RequestMethod.GET)
	public List<Producto> obtenerProductos() {
		return prodServicio.consultarTodos();
	}

	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "/crear", method = RequestMethod.POST)
	public void crearProducto(@RequestBody Producto objProducto) {
		prodServicio.agregar(objProducto);
	}

	@ResponseStatus(code = HttpStatus.OK, reason = "Producto Eliminado.")
	@RequestMapping(value = "/borrar/{codigo}", method = RequestMethod.DELETE)
	public void borrarProducto(@PathVariable Long codigo) {
		prodServicio.eliminar(codigo);
	}

	@ResponseStatus(code = HttpStatus.ACCEPTED, reason = "Producto Actualizado Correctamente.")
	@RequestMapping(value = "/actualizar", method = RequestMethod.PUT)
	public void actualizarProducto(@RequestBody Producto objProducto) {
		prodServicio.actualizar(objProducto);
	}

}
