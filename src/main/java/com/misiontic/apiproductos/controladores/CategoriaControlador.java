package com.misiontic.apiproductos.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.misiontic.apiproductos.entidades.Categoria;
import com.misiontic.apiproductos.servicios.CategoriaServicio;

@RestController
@RequestMapping("/categorias")
@CrossOrigin(origins = "*")
public class CategoriaControlador {
	
	@Autowired
	private CategoriaServicio cateServicio;
	
	@RequestMapping(value = "/todas", method = RequestMethod.GET)
	public List<Categoria> obtenerCategorias(){
		return cateServicio.consultarTodos();
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "/crear", method = RequestMethod.POST, consumes = "application/json")
	public void crearCategoria(@RequestBody Categoria objCategoria) {
		cateServicio.agregar(objCategoria);
	}
	
	@ResponseStatus(code = HttpStatus.OK, reason = "Categoria Eliminada.")
	@RequestMapping(value = "/borrar/{codigo}", method = RequestMethod.DELETE)
	public void borrarCategoria(@PathVariable Long codigo) {
		cateServicio.eliminar(codigo);
	}
	
	@ResponseStatus(code = HttpStatus.ACCEPTED, reason = "Categoria Actualizada Correctamente.")
	@RequestMapping(value = "/actualizar", method = RequestMethod.PUT)
	public void actualizarCategoria(@RequestBody Categoria objCategoria) {
		cateServicio.actualizar(objCategoria);
	}

}
