package com.misiontic.apiproductos.servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.misiontic.apiproductos.entidades.Categoria;
import com.misiontic.apiproductos.entidades.Producto;
import com.misiontic.apiproductos.interfaces.Operaciones;
import com.misiontic.apiproductos.repositorios.ProductoRepositorio;

@Service
public class ProductoServicio implements Operaciones<Producto> {

	@Autowired
	private ProductoRepositorio prodRepo;

	@Override
	public boolean agregar(Producto objeto) {
		Producto objTemporal = prodRepo.save(objeto);
		return objTemporal != null;
	}

	@Override
	public List<Producto> consultarTodos() {
		return prodRepo.findAll();
	}

	@Override
	public boolean eliminar(Long codigo) {
		prodRepo.deleteById(codigo);
		return !prodRepo.existsById(codigo);
	}

	@Override
	public boolean actualizar(Producto objeto) {
		Optional<Producto> objetoVerificado = prodRepo.findById(objeto.getIdProducto());

		if (!objetoVerificado.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "El producto no existe.");
		} else {
			prodRepo.save(objeto);
			return true;
		}
	}

	@Override
	public Integer cantidadRegistros() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Producto buscar(Long codigo) {
		// TODO Auto-generated method stub
		return null;
	}

}
