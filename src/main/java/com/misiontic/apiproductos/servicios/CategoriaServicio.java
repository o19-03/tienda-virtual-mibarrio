package com.misiontic.apiproductos.servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.misiontic.apiproductos.entidades.Categoria;
import com.misiontic.apiproductos.interfaces.Operaciones;
import com.misiontic.apiproductos.repositorios.CategoriaRepositorio;

@Service
public class CategoriaServicio implements Operaciones<Categoria> {

	@Autowired
	private CategoriaRepositorio cateRepo;

	@Override
	public boolean agregar(Categoria objeto) {
		Categoria objCategoria = cateRepo.save(objeto);
		return objCategoria != null;
	}

	@Override
	public List<Categoria> consultarTodos() {
		return cateRepo.findAll();
	}

	@Override
	public boolean eliminar(Long codigo) {
		cateRepo.deleteById(codigo);
		return !cateRepo.existsById(codigo);
	}

	@Override
	public boolean actualizar(Categoria objeto) {
		Optional<Categoria> objetoVerificado = cateRepo.findById(objeto.getIdCategoria());
		
		if (!objetoVerificado.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "La categoria no existe.");
		}else {
			cateRepo.save(objeto);
			return true;
		}
	}

	@Override
	public Integer cantidadRegistros() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Categoria buscar(Long codigo) {
		// TODO Auto-generated method stub
		return null;
	}

}
